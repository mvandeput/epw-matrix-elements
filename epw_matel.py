#!/usr/bin/env python3
""" EPW Matrix Elements

Usage:
  epw_matel.py
  epw_matel.py plot
  epw_matel.py interpolate (el | ph | DK) <points>
  epw_matel.py export (el | ph)
  epw_matel.py export DK <bands>...
  epw_matel.py (-h | --help)
  epw_matel.py --version
"""
# this code reads:  .freq, .ephmat_kvec1, .ephmat_klen1, .ephmat1

from docopt import docopt
from struct import unpack

import numpy as np
from scipy import interpolate


def guess_nrecords(fd):
    start = fd.tell()
    raw = fd.read(4)
    reclen = int(unpack('i', raw)[0])
    fd.seek(0, 2)
    nbytes = fd.tell() - start
    fd.seek(start)
    return reclen, nbytes // (reclen + 4 + 4)


def read_record(fd):
    raw = fd.read(4)
    if len(raw) != 4:
        raise EOFError
    reclen = int(unpack('i', raw)[0])
    raw = fd.read(reclen)
    bkscp = int(unpack('i', fd.read(4))[0])
    assert(reclen == bkscp)
    return raw


def read_freq(folder, prefix):
    '''reads the q-points from the .freq file'''
    with open(folder + '/' + prefix + '.freq', 'rb') as f:
        reclen, nq, nmodes, bkscp = tuple(np.array(unpack('i' * 4,
                                                          f.read(4 * 4)),
                                                   dtype=int))
        q = np.empty((nq, 3))
        omega = np.empty((nq, nmodes))
        for iq in range(nq):
            f.read(4)
            q[iq, :] = np.array(unpack('d' * 3, f.read(3 * 8)))
            f.read(4)
            for im in range(nmodes):
                f.read(4)
                omega[iq, im] = np.array(unpack('d', f.read(8)))[0]
                f.read(4)

        return q, omega


def read_kvec1(folder, prefix):
    fname = folder + '/' + prefix + '.ephmat_kvec1'
    with open(fname, 'rb') as fd:
        reclen, N = guess_nrecords(fd)

    # memory map trick: reclen, ik, iq, ibnd, jbnd, bksp (reclen again)
    record = [('reclen', 'i4'),
              ('ik', 'i4'),
              ('imode', 'i4'),
              ('iq', 'i4'),
              ('ibnd', 'i4'),
              ('jbnd', 'i4'),
              ('bksp', 'i4')]

    mmap = np.memmap(fname, mode='r', shape=N, dtype=record)
    ik = mmap['ik']
    iq = mmap['iq']
    imode = mmap['imode']
    ibnd = mmap['ibnd']
    jbnd = mmap['jbnd']
    return ik, iq, imode, ibnd, jbnd


def read_klen1(folder, prefix):
    fname = folder + '/' + prefix + '.ephmat_klen1'
    with open(fname, 'rb') as fd:
        data = read_record(fd)  # 1st record = number of k points
        nk = int(unpack('i', data)[0])
        data = read_record(fd)  # 2nd record = number of bands
        nbnd = int(unpack('i', data)[0])
        offset = fd.tell()
    # memory map
    record = [('reclen', 'i4'),
              ('E', 'f8'),
              ('bksp', 'i4')]
    mmap = np.memmap(fname, mode='r', offset=offset,
                     shape=(nk, nbnd),
                     dtype=record)
    E = mmap['E']
    return E


def read_ephmat1(folder, prefix):
    '''reads the electon-phonon matrix elements from the .ephmat1 file'''
    fname = folder + '/' + prefix + '.ephmat1'
    with open(fname, 'rb') as fd:
        read_record(fd)  # first record contains some sizes we don't need
        offset = fd.tell()
        reclen, N = guess_nrecords(fd)

    record = [('reclen', 'i4'),
              ('DK', 'f8'),
              ('bksp', 'i4')]
    mmap = np.memmap(fname, mode='r', offset=offset, shape=N, dtype=record)
    DK = mmap['DK']
    return DK


def interp_freq(q, omega, q_interp):
    omega_interp = np.empty(q_interp.shape[0], omega.shape[1])
    for imode in range(omega.shape[1]):
        omega_interp[:, imode] = interpolate.griddata(q, omega[:, imode],
                                                      q_interp)
    return omega_interp


def plot_freq(q, omega):
    from mpl_toolkits.mplot3d import Axes3D  # noqa: F401
    from itertools import cycle
    import matplotlib.pyplot as plt
    fig = plt.figure(dpi=150)
    ax = fig.add_subplot(111, projection='3d')
    colors = cycle(['r', 'g', 'b', 'k'])
    qplt = np.mod(q + 0.5, 1.0) - 0.5
    for imode, color in zip(range(omega.shape[1]), colors):
        ax.scatter(qplt[:, 0], qplt[:, 1], omega[:, imode],
                   color=color)


def confirm_mem(shape):
    print('MEM of interpolated: {} MB'.format(np.prod(shape) * 8 / 1024**2))
    response = input("Are you sure? [y/N] ")
    if not response.lower().startswith('y'):
        exit()
    return np.empty(shape)


def fortran_write(fname, data, endian='<'):
    shape = (data.shape[0],)
    data_dtype = data.dtype.newbyteorder(endian)
    dtype = [('reclen', endian + 'i4'),
             ('data', str(data_dtype), data.shape[1]),
             ('bkscp', endian + 'i4')]
    reclen = data_dtype.itemsize * data.shape[1]
    mm = np.memmap(fname, mode='w+', dtype=dtype, shape=shape)
    mm['reclen'][:] = reclen
    mm['data'][:] = data
    mm['bkscp'][:] = reclen
    mm.flush()


if __name__ == '__main__':

    args = docopt(__doc__, version='')

    print(args)

    print('# reading .freq')
    q, omega = read_freq('.', 'calc')
    k = q

    print('# reading .ephmat_kvec1')
    ik, iq, imode, ibnd, jbnd = read_kvec1('.', 'calc')

    print('# reading .ephmat_klen1')
    E = read_klen1('.', 'calc')

    print('# reading .ephmat1')
    DK = read_ephmat1('.', 'calc')

    nq, nmodes = omega.shape
    nk, nbands = E.shape

    print('Input from EPW')
    qmin, qmax = q.min(axis=0), q.max(axis=0)
    print('  q {}: {} to {}'.format(q.shape, qmin, qmax))
    print('  omega {}: {} to {}'.format(omega.shape, omega.min(), omega.max()))
    ikmax, ikmin = ik.max(), ik.min()
    print('  ik {}: {} to {}'.format(ik.shape, ikmin, ikmax))
    iqmax, iqmin = iq.max(), iq.min()
    print('  iq {}: {} to {}'.format(iq.shape, iqmin, iqmax))
    imodemax, imodemin = imode.max(), imode.min()
    print('  imode {}: {} to {}'.format(imode.shape, imodemin, imodemax))
    ibndmax, ibndmin = ibnd.max(), ibnd.min()
    print('  ibnd {}: {} to {}'.format(ibnd.shape, ibndmin, ibndmax))
    jbndmax, jbndmin = jbnd.max(), jbnd.min()
    print('  jbnd {}: {} to {}'.format(jbnd.shape, jbndmin, jbndmax))
    print('  E {}: {} to {}'.format(E.shape, E.min(), E.max()))
    print('  DK {}: {} to {}'.format(DK.shape, DK.min(), DK.max()))

    # checks
    assert(q.shape == (nq, 3))
    assert(DK.shape == ik.shape)
    assert(ikmin == 1)
    assert(ikmax == nk)
    assert(iqmin == 1)
    assert(iqmax == nq)
    assert(imodemin == 1)
    assert(imodemax == nmodes)
    assert(ibndmin == 1)
    assert(ibndmax == nbands)
    assert(jbndmin == 1)
    assert(jbndmax == nbands)

    realdims = [ii for ii, (mini, maxi) in enumerate(zip(qmin, qmax))
                if mini != maxi]
    offsets = ([np.zeros(3)] +
               [np.array([1.0 if d == dim else 0.0 for d in range(3)])
                for dim in realdims])
    dimnames = ['x', 'y', 'z']
    print('Detected dimensions: {}'.format([dimnames[d] for d in realdims]))

    if args['plot']:
        import matplotlib.pyplot as plt
        plot_freq(q, omega)
        plt.suptitle('phonons')
        plot_freq(q, E)
        plt.suptitle('electrons')
        plt.show()

    def get_grid(idx):
        ix = np.unique(idx[:, 0])
        iy = np.unique(idx[:, 1])
        iz = np.unique(idx[:, 2])
        shape = ix.shape + iy.shape + iz.shape + idx.shape[-1:]
        mgrid = np.stack(np.meshgrid(ix, iy, iz, indexing='ij'), axis=-1)
        order = 'C'
        grid = idx.reshape(shape, order=order)
        assert(np.all(grid == mgrid))
        return shape[:-1]

    shapeq = get_grid(q)
    shapek = get_grid(k)

    if args['export']:

        if args['ph']:
            fortran_write('export_ph', omega, endian='>')
            fmt = '(qx, qy, qz; imode) = ({q[0]}, {q[1]}, {q[2]}; {modes})'
            print('output in export_ph')
            print(fmt.format(q=shapeq, modes=nmodes))

        if args['el']:
            fortran_write('export_el', E, endian='>')
            fmt = '(kx, ky, kz; iband) = ({k[0]}, {k[1]}, {k[2]}; {bands})'
            print('output in export_el')
            print(fmt.format(k=shapek, bands=nbands))

        if args['DK']:
            from functools import reduce
            ibands = np.array(list(map(int, args['<bands>'])))
            xnbands = len(ibands)
            xDK = np.zeros((nk, nq, xnbands, xnbands, nmodes))
            imask = reduce(np.logical_or, [ibnd == ii for ii in ibands])
            jmask = reduce(np.logical_or, [jbnd == ii for ii in ibands])
            mask = np.logical_and(imask, jmask)
            N = np.sum(mask)
            xibnd = np.empty(N, dtype=int)
            xjbnd = np.empty(N, dtype=int)
            for jj, ii in enumerate(ibands):
                xibnd[ibnd[mask] == ii] = jj
                xjbnd[jbnd[mask] == ii] = jj
            xDK[ik[mask]-1, iq[mask]-1, xibnd, xjbnd, imode[mask]-1] = DK[mask]
            fmt = ('(kx, ky, kz, qx, qy, qz, ibnd, jbnd; imode) = '
                   '({k[0]}, {k[1]}, {k[2]}, {q[0]}, {q[1]}, {q[2]}, '
                   '{bands}, {bands}; {modes})')
            fname = 'export_dk_' + '_'.join(map(str, ibands))
            fortran_write(fname, xDK.reshape((-1, nmodes)), endian='>')
            print('output in ' + fname)
            print(fmt.format(k=shapek, q=shapeq, modes=nmodes,
                             bands=len(ibands)))

    if args['interpolate']:
        # load in interpolation points
        fname = args['<points>']
        print("# reading " + fname)

        if args['ph']:
            qpoints = np.loadtxt(fname, skiprows=1, usecols=(0, 1, 2))
            qpoints = np.mod(qpoints, 1.0)
            nqpoints = qpoints.shape[0]

        if args['DK'] or args['el']:
            kpoints = np.loadtxt(fname, skiprows=3, usecols=(0, 1, 2))
            nkpoints = kpoints.shape[0]
        if args['DK']:
            kpoints = kpoints[:, None, :] * (kpoints[None, :, :]**0)
            qpoints = kpoints[:, None, :] - kpoints[None, :, :]
            qpoints = np.mod(qpoints, 1.0)
        if args['el'] or args['DK']:
            kpoints = np.mod(kpoints, 1.0)

        # interpolate
        if args['ph']:
            interp = confirm_mem((nqpoints, nmodes))
            qepw = np.concatenate([q + offset[None, :] for offset in offsets],
                                  axis=0)
            omegaepw = np.concatenate([omega for offset in offsets], axis=0)
            for iimode in range(nmodes):
                interp[:, iimode] = interpolate.griddata(qepw[:, realdims],
                                                         omegaepw[:, iimode],
                                                         qpoints[:, realdims],
                                                         fill_value=0.0)
            # np.save('{}_ph.npy'.format(fname), interp)
            fortran_write('{}_ph'.format(fname),
                          interp,
                          endian='>')

        if args['el']:
            interp = confirm_mem((nkpoints, nbands))
            kepw = np.concatenate([q + offset[None, :] for offset in offsets],
                                  axis=0)
            Eepw = np.concatenate([E for offset in offsets], axis=0)
            for iibnd in range(nbands):
                interp[:, iibnd] = interpolate.griddata(kepw[:, realdims],
                                                        Eepw[:, iibnd],
                                                        kpoints[:, realdims],
                                                        fill_value=0.0)
            # np.save('{}_el.npy'.format(fname), interpolated)
            fortran_write('{}_el'.format(fname),
                          interp,
                          endian='>')

        if args['DK']:
            intpoints = np.concatenate((kpoints[:, realdims],
                                        qpoints[:, realdims]), axis=1)
            interpolated = confirm_mem((nkpoints, nkpoints,
                                        nbands, nbands, nmodes))
            for iimode in range(nmodes):
                filt = imode == iimode + 1
                for iibnd in range(nbands):
                    filt1 = ibnd[filt] == iibnd + 1
                    for jjbnd in range(nbands):
                        filt2 = jbnd[filt][filt1] == jjbnd + 1
                        iiq = iq[filt][filt1][filt2] - 1
                        iik = ik[filt][filt1][filt2] - 1
                        dk = DK[filt][filt1][filt2]
                        points = np.concatenate((k[iik, :][:, realdims],
                                                 q[iiq, :][:, realdims]),
                                                axis=1)
                        print('interpolating {} to {}'.format(points.shape,
                                                              intpoints.shape))
                        dkinterp = interpolate.griddata(points,
                                                        dk,
                                                        intpoints,
                                                        method='nearest',
                                                        fill_value=0.0)
                        dkinterp.shape = (nkpoints, nkpoints)
                        interpolated[:, :, iibnd, jjbnd, iimode] = dkinterp

            # np.save('{}_DK.npy'.format(fname), interpolated)
            fortran_write('{}_DK'.format(fname),
                          interpolated.reshape((-1, nmodes)),
                          endian='>')
