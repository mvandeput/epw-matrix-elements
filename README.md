# Extract electron-phonon matrix elements from EPW

Tools for extracting the matrix elements from the Quantum Espresso EPW package.
Currently, tools are only provided for QE 6.2.1.

## Apply the patch

To extract matrix elements, the QE 6.2.1 source needs to be patched:

```console
$ ls
qe-6.2.1.epw.ephmat.patch   qe-6.2.1/
$ patch -p0 < qe-6.2.1.epw.ephmat.patch
patching file qe-6.2.1/EPW/src/write_ephmat.f90
patching file qe-6.2.1/EPW/src/io_epw.f90
```

After patching, you should be able to compile QE as usual.

## Python script

A Python 3 script `epw_matel.py` is provided to read the matrix elements and
perform basic file-transformations on them. Prerequisite python packages are
`docopt`, `numpy` and `scipy`.